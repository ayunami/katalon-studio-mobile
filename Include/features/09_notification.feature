@Notification
Feature: Notification

  Scenario: TC-Notif-001 user wants to access the notification menu
    Given user successfully log in and already on homepage
    When user tap menu notifikasi on bottom bar dashboard
    Then the system will display a list of notifications received by the user
