@Profile
Feature: profile

   Background: TC.Log.002.001 - user has successfully logged in with valid information
    Given user is already on the login page
    When user input valid credentials and click login button
    Then user has successfully login and is redirected to the account page

  Scenario: TC.Upd.Prof.001.001 - user can update profile
    Given user click edit button
    When user click image button
    And user click galerry button
    And user select image
    Then user get pop-up message profil berhasil diperbaharui

  Scenario Outline: user can not update profile
    Given user click edit button
    When user click <field> field
    And user update profile but <condition>
    And user click simpan button
    Then user get pop-up message error <result>

    Examples: 
      | case_id             |  field    | condition                    | result      |
      | TC.Upd.Prof.001.002 | nama      | leave nama field empty       | wajib diisi |
      | TC.Upd.Prof.001.003 | nomor hp  | leave nomor hp field empty   | wajib diisi |
      | TC.Upd.Prof.001.004 | alamat    | input spaces in alamat field | wajib diisi |
