@EditProduct
Feature: edit product

  Background: TC.Log.002.001 - user has successfully logged in with valid information
    Given user is already on the login page
    When user input valid credentials and click login button
    Then user has successfully login and is redirected to the account page

  Scenario: TCM.Edit.001.001 user is succesfully edit product data
    Given user in account page
    And user tap menu daftar jual saya
    And user choose desired product to edit
    When user edit the product data
    And user change product picture below 5 mb size
    And user tap perbarui button
    Then user successfully edit product data

  Scenario: TCM.Edit.001.002 user change product picture above 10 mb size
    Given user in account page
    And user tap menu daftar jual saya
    And user choose desired product to edit
    When user edit the product data
    And user change product picture above 5 mb size
    And user tap perbarui button
    Then user will not successfully edit product data
