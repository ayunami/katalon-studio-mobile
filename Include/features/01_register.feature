@Register
Feature: register

  Scenario: TC.Reg.001.001 user has successfully registered with valid information
    Given user is already on the registration page
    When user input valid information and click register button
    Then user has successfully registers and is redirected to the account page