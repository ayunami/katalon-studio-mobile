@BuyProduct
Feature: buy product

  Scenario: TS.Buy.001.001 user want to buy product after login
    Given user login with valid credentials
    When user go to homepage
    And user choose the product that want to buy
    And user submit offering product
    Then user successfully buy the product

  Scenario: TS.Buy.001.002 user want to buy product before login
    Given user already on homepage
    And user choose the product
    And user click offer button
    Then user can see alert message that user must login first

  Scenario: TS.Buy.001.003 user want to buy product with 0 on the offer page
    Given user already login
    When user choose the product to buy
    And user input the nominal of product offer with 0 and click submit
    Then user stay in the product page
