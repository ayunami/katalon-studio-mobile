@SellProduct
Feature: sell product

  Scenario: TS.Sell.001.001 user want to sell product after login
    Given user login as seller
    When user view the notification
    And user choose product offer
    And user accept the offer from buyer
    Then user stay on the offer page

  Scenario: TS.Sell.001.002 user want to sell product before login
    Given user go to home page
    When user click the sell button
    Then user redirected to login page that user must login first
