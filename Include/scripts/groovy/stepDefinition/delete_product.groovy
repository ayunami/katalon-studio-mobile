package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.security.PublicKey

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then
import org.openqa.selenium.Keys as Keys


public class delete_product {
	@Given("user successfully log in and already on akun menu")
	public void user_successfully_log_in_and_already_on_akun_menu() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
		Mobile.delay(3)
		Mobile.tap(findTestObject('Object Repository/homepage/button_akun'), 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/input_email'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/input_password'), 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/input_email'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/input_password'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.setText(findTestObject('Object Repository/login/input_email'), 'rendy123@gmail.com', 0)
		Mobile.setText(findTestObject('Object Repository/login/input_password'), '123456', 0)
		Mobile.tap(findTestObject('Object Repository/login/button_hide_password'), 0)
		Mobile.tap(findTestObject('Object Repository/login/button_masuk'), 0)
		Mobile.delay(3)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/button daftar jual saya'), 0)
	}


	@When("user on already to sub menu produk")
	public void user_on_already_to_sub_menu_produk() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/menu_daftar_jual_saya'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/list produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/product_card'), 0)
	}

	@When("user tap symbol recycle bin on product")
	public void user_tap_symbol_recycle_bin_on_product() {
		Mobile.tap(findTestObject('Object Repository/delete product/button delete'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/pop up delete'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/button hapus'), 0)
	}

	@When("user select and tap hapus")
	public void user_select_and_tap_hapus() {
		Mobile.tap(findTestObject('Object Repository/delete product/button hapus'), 0)
	}

	@Then("the system will delete the product and display allert produk berhasil dihapus")
	public void the_system_will_delete_the_product_and_display_allert_produk_berhasil_dihapus() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/allert_berhasil dihapus'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/list diminati'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/list produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/delete product/list terjual'), 0)
	}
}
