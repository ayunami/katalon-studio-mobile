package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class edit_product {
	@And("user tap menu daftar jual saya")
	public void user_tap_menu_daftar_jual_saya() {
		Mobile.tap(findTestObject('Object Repository/my_account/button_daftar_jual_saya'), 0)
	}

	@And("user choose desired product to edit")
	public void user_choose_desired_product_to_edit() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_diminati'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_terjual'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/product_card'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/button_delete_product1'), 0)
		Mobile.tap(findTestObject('Object Repository/daftar_jual_saya/product_card'), 0)
	}

	@When("user edit the product data")
	public void user_edit_the_product_data() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/edit_product/edit_input_nama_produk'), 2)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_nama_produk'), 0)
		Mobile.clearText(findTestObject('Object Repository/edit_product/edit_input_nama_produk'), 0)
		Mobile.setText(findTestObject('Object Repository/edit_product/edit_input_nama_produk'), 'Samsung Galaxy Z Fold', 0)

		Mobile.waitForElementPresent(findTestObject('Object Repository/edit_product/edit_input_harga_produk'), 2)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_harga_produk'), 0)
		Mobile.clearText(findTestObject('Object Repository/edit_product/edit_input_harga_produk'), 0)
		Mobile.setText(findTestObject('Object Repository/edit_product/edit_input_harga_produk'), '12000000', 0)

		Mobile.waitForElementPresent(findTestObject('Object Repository/edit_product/dropdown_edit_kategori'), 3)
		Mobile.tap(findTestObject('Object Repository/edit_product/dropdown_edit_kategori'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/kategori_2'), 0)

		Mobile.waitForElementPresent(findTestObject('Object Repository/edit_product/edit_input_lokasi'), 2)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_lokasi'), 0)
		Mobile.clearText(findTestObject('Object Repository/edit_product/edit_input_lokasi'), 0)
		Mobile.setText(findTestObject('Object Repository/edit_product/edit_input_lokasi'), 'Jakarta Timur', 0)

		Mobile.waitForElementPresent(findTestObject('Object Repository/edit_product/edit_input_deskripsi'), 3)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_deskripsi'), 0)
		Mobile.clearText(findTestObject('Object Repository/edit_product/edit_input_deskripsi'), 0)
		Mobile.setText(findTestObject('Object Repository/edit_product/edit_input_deskripsi'), 'Samsung Galaxy Z Fold Second Mulus Like New', 0)
	}

	@And("user change product picture below 5 mb size")
	public void user_change_product_picture_below_5mb_size() {
		Mobile.tap(findTestObject('Object Repository/edit_product/button_foto_produk'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/button_to_gallery2'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/product_image2'), 0)
	}

	@And("user change product picture above 5 mb size")
	public void user_change_product_picture_above_5mb_size() {
		Mobile.tap(findTestObject('Object Repository/edit_product/button_foto_produk'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/button_to_gallery2'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/product_image3'), 0)
	}

	@And("user tap perbarui button")
	public void user_tap_perbarui_button() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/button_perbarui_produk'), 0)
		Mobile.tap(findTestObject('Object Repository/edit_product/button_perbarui_produk'), 0)
	}

	@Then("user successfully edit product data")
	public void user_successfully_edit_product_data() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/message_produk_diperbarui'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/daftar_jual_saya/product_card2'), 0)
	}

	@Then("user will not successfully edit product data")
	public void user_will_not_successfully_edit_product_data() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/message_entity_too_large'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_nama_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_harga_produk'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/dropdown_edit_kategori'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_lokasi'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/edit_product/edit_input_deskripsi'), 0)
	}
}
